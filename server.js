const express = require('express');
const mysql = require('mysql2/promise'); // Use the mysql2/promise library for async/await support
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken')
const app = express();
const port = 3000;
const path = require('path')
const bcrypt = require('bcrypt')
const session = require('express-session'); // Import express-session

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const pool = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: 'insert_password',
  database: 'estate_db'
});

app.use(express.static(__dirname + '/public'));

app.use(
  session({
    secret: 'Ax84Ma98Mi03', 
    resave: false,
    saveUninitialized: false,
  })
);


app.post('/register', async (req, res) => {
  try {
    const { name, email, password, c_password, phone } = req.body;

    if (password !== c_password) {
      return res.status(400).json({ message: 'Passwords do not match.' });
    }

    const saltRounds = 10; 
    const hashedPassword = await bcrypt.hash(password, saltRounds);

    const connection = await pool.getConnection();

    const [rows] = await connection.execute(
      'INSERT INTO users (name, email, password, number) VALUES (?, ?, ?, ?)',
      [name, email, hashedPassword, phone]
    );

    connection.release();

    res.status(201).json({ message: 'User registered successfully!' });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while registering the user.' });
  }
});

app.get('/users', async (req, res) => {
  try {
    const connection = await pool.getConnection();

    const [rows] = await connection.execute('SELECT * FROM users');

    connection.release();

    res.status(200).json({ users: rows });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while fetching users.' });
  }
});

app.get('/user/:id?', async (req, res) => {
  try {
    const userId = req.params.id;

    const connection = await pool.getConnection();

    if (userId) {
      
      const [userRows] = await connection.execute('SELECT * FROM users WHERE id = ?', [userId]);
      connection.release();

      if (userRows.length === 1) {
        res.status(200).json({ user: userRows[0] });
      } else {
        res.status(404).json({ message: 'User not found.' });
      }
    } else {
      // If no userID is provided, get all users
      const [rows] = await connection.execute('SELECT * FROM users');
      connection.release();
      res.status(200).json({ users: rows });
    }
  } catch (error) {
    console.error('Error fetching users:', error);
    res.status(500).json({ message: 'An error occurred while fetching users.' });
  }
});

// Assuming you have already defined your Express app and the 'pool' object for database connections

app.delete('/delete-user/:id', async (req, res) => {
  try {
    const userIdToDelete = req.params.id;
    const loggedInUserId = req.session.user.userId; // Assuming you have the user ID stored in the session

    if (userIdToDelete === loggedInUserId) {
      return res.status(403).json({ message: 'You cannot delete your own account.' });
    }

    const connection = await pool.getConnection();

    const [rows] = await connection.execute(
      'DELETE FROM users WHERE id = ? AND isAdmin = 0',
      [userIdToDelete]
    );

    connection.release();

    if (rows.affectedRows === 0) {
      // No user found with the given ID or the user has isAdmin set to 1
      return res.status(404).json({ message: 'User not found or cannot delete an admin user.' });
    }

    res.status(200).json({ message: 'User deleted successfully!' });
  } catch (error) {
    res.status(500).json({ message: 'An error occurred while deleting the user.' });
  }
});




  requireLogin = (req, res, next) => {
  if (req.session.user) {
    next(); 
  } else {
    res.status(401).json({ message: 'User is not logged in' });
  }
}

app.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;

    const connection = await pool.getConnection();

    const [userRows] = await connection.execute('SELECT * FROM users WHERE email = ?', [email]);

    if (userRows.length === 1) {
      const user = userRows[0];
      const storedPassword = user.password;
      const passwordsMatch = await bcrypt.compare(password, storedPassword);

      if (passwordsMatch) {
        if (req.session.user) {
          res.status(200).json({ message: 'User is already logged in' });
        } else {
          req.session.user = {
            userId: user.id,
            email: user.email,
            isAdmin: user.isAdmin === 1, // Adjust based on your boolean representation
          };
          if (req.session.user.isAdmin) {
            res.redirect('admin.html');
          } else {
            res.redirect('profile.html');
          }

        }
      } else {
        res.status(401).json({ message: 'Login failed: Invalid password' });
      }
    } else {
      res.status(401).json({ message: 'Login failed: User not found' });
    }

    connection.release();
  } catch (error) {
    console.error(`Login error: ${error}`);
    res.status(500).json({ message: 'An error occurred while processing the login' });
  }
});




app.get('/logout', requireLogin, (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      console.error(err);
    }
    res.redirect('/login.html'); 
  });
});

app.post('/create-ad', requireLogin, async (req, res) => {
  try {
    const { status, property_type, title, description, price, location, image_url } = req.body;

    const user_id = req.session.user.userId;
    const isAdmin = req.session.user.isAdmin;

    const [result] = await pool.execute(
      'INSERT INTO ads (title, description, price, location, image_url, status, property_type, user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?)',
      [title, description, price, location, image_url, status, property_type, user_id]
    );

    // Check if the user is an admin
    if (isAdmin) {
      // Redirect to admin.html if the user is an admin
      res.status(201).json({ message: 'Advertisement created successfully by admin', redirectTo: 'public/admin.html' });
    } else {
      // Redirect to listings.html if the user is not an admin
      res.status(202).json({ message: 'Advertisement created successfully by user', redirectTo: 'public/listings.html' });
    }
  } catch (error) {
    console.error('Error creating advertisement:', error);
    res.status(500).json({ message: 'An error occurred while creating the advertisement' });
  }
});


app.get('/getAds', async (req, res) => {
  try {
    const connection = await pool.getConnection();
    const [rows] = await connection.query('SELECT * FROM ads');
    connection.release();
    res.status(200).json(rows);
  } catch (error) {
    console.error('Error fetching ads:', error);
    res.status(500).json({ error: 'An error occurred while fetching ads.' });
  }
});

app.get('/getAd/:adId', async (req, res) => {
  try {
    const adId = req.params.adId;
    const connection = await pool.getConnection();
    const [rows] = await connection.execute('SELECT * FROM ads WHERE id = ?', [adId]);
    connection.release();

    if (rows.length === 0) {
      return res.status(404).json({ error: 'Ad not found' });
    }

    res.status(200).json(rows[0]); 
  } catch (error) {
    console.error('Error fetching ad:', error);
    res.status(500).json({ error: 'An error occurred while fetching the ad.' });
  }
});

app.put('/update-ad/:adId', requireLogin, async (req, res) => {
  try {
    const adId = req.params.adId;
    const userId = req.session.user.userId;

    // Check if the user is an admin
    const isAdmin = req.session.user.isAdmin || false;

    // Use the appropriate method based on whether the user is an admin
    if (isAdmin) {
      // If the user is an admin, use the method without checking the user's ID
      await updateAdWithoutUserId(req.body, adId);
    } else {
      // If the user is not an admin, check the user's ID before updating
      const [ad] = await pool.execute('SELECT * FROM ads WHERE id = ? AND user_id = ?', [adId, userId]);

      if (ad.length === 0) {
        return res.status(401).json({ message: 'Unauthorized: You do not own this ad or the ad does not exist.' });
      }

      await updateAdWithoutUserId(req.body, adId);
    }

    res.status(200).json({ message: 'Advertisement updated successfully' });
  } catch (error) {
    console.error('Error updating advertisement:', error);
    res.status(500).json({ message: 'An error occurred while updating the advertisement' });
  }
});


async function updateAdWithoutUserId(updateData, adId) {
  
  const updateFields = Object.keys(updateData).map(field => `${field} = ?`);
  const updateValues = Object.values(updateData);
  updateValues.push(adId);

  const updateQuery = `UPDATE ads SET ${updateFields.join(', ')} WHERE id = ?`;
  await pool.execute(updateQuery, updateValues);
}


app.delete('/delete-ad/:adId', requireLogin, async (req, res) => {
  try {
    const adId = req.params.adId;
    const userId = req.session.user.userId;
    
    // Check if the user is an admin
    const isAdmin = req.session.user.isAdmin || false;

    // Use the appropriate method based on whether the user is an admin
    if (isAdmin) {
      // If the user is an admin, use the method without checking the user's ID
      await deleteAdWithoutUserId(adId);
    } else {
      // If the user is not an admin, check the user's ID before deleting
      const [ad] = await pool.execute('SELECT * FROM ads WHERE id = ? AND user_id = ?', [adId, userId]);

      if (ad.length === 0) {
        return res.status(401).json({ message: 'Unauthorized: You do not own this ad or the ad does not exist.' });
      }

      await deleteAdWithoutUserId(adId);
    }

    res.status(200).json({ message: 'Advertisement deleted successfully' });
  } catch (error) {
    console.error('Error deleting advertisement:', error);
    res.status(500).json({ message: 'An error occurred while deleting the advertisement' });
  }
});

// Separate method for deleting an ad without checking the user's ID
async function deleteAdWithoutUserId(adId) {
  // Delete the ad without checking the user's ID
  await pool.execute('DELETE FROM ads WHERE id = ?', [adId]);
}


app.get('/user-ads', requireLogin, async (req, res) => {
  try {
    const userId = req.session.user.userId; // Get the logged-in user's ID

    const [userAds] = await pool.execute('SELECT * FROM ads WHERE user_id = ?', [userId]);

    res.status(200).json({ ads: userAds });
  } catch (error) {
    console.error('Error retrieving user ads:', error);
    res.status(500).json({ message: 'An error occurred while retrieving user ads' });
  }
});

app.get('/user-ads/:userId', async (req, res) => {
  try {
    const userId = req.params.userId; // Get the user ID from the URL parameter

    const connection = await pool.getConnection();
    const [userAds] = await connection.execute('SELECT * FROM ads WHERE user_id = ?', [userId]);
    connection.release();

    res.status(200).json({ ads: userAds });
  } catch (error) {
    console.error('Error retrieving user ads:', error);
    res.status(500).json({ message: 'An error occurred while retrieving user ads' });
  }
});

app.get('/profile', requireLogin,  async (req, res) => {
  console.log(req.session.user)
  try {
  
    const userId = req.session.user.userId; 
    const connection = await pool.getConnection();
    const [rows] = await connection.execute('SELECT name, email, number FROM users WHERE id = ?', [userId]);
    connection.release();

    if (rows.length === 1) {
      const user = rows[0];
      res.json(user);
    } else {
      res.status(404).json({ message: 'User not found' });
    }
  } catch (error) {
    console.error(`Profile error: ${error}`);
    res.status(500).json({ message: 'An error occurred while fetching the profile' });
  }
});

app.get('/profile.html', requireLogin, (req, res) => {
  res.sendFile(__dirname + '/public/profile.html');
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});