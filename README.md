- proiect full stack
- site de imobiliare (anunturi)
- drepturi de admin
- operatii CRUD (sql+NodeJS&ExpressJS)
- VanillaJS
- Register/Login
- Cum adaug anunturi


What to implement:
    - post formular
    - connecting ads to db
    - every user can have 0 or more ads (one to many, or zero to many?)
    - login session
    - admin control panel
